pragma solidity ^0.4.21;
/*pragma experimental ABIEncoderV2;*/

/*import "./Owned.sol";*/


contract Deal {
    event LogNewPayment(uint id, uint price, uint amount);

    struct orderedProduct {
        uint id;
        uint price;
        uint orderedAmount;
    }

    address initiator;
    orderedProduct[] public orderedProducts;
    string addressee;

    function Deal(
        uint[] ids,
        uint[] prices,
        uint[] amounts,
        string manufacturer
    )
    public {
        initiator = msg.sender;
        addressee = manufacturer;

        for (uint i; i < ids.length; i++) {
            orderedProducts.push(orderedProduct(ids[i], prices[i], amounts[i]));
        }
    }

    function getCountProducts() public constant returns (uint) {
        return orderedProducts.length;
    }

    function getProduct(uint index) public constant returns (uint, uint, uint) {
        /* No control over index range */
        return (orderedProducts[index].id, orderedProducts[index].price, orderedProducts[index].orderedAmount);
    }

    /*function sendEther() public {
        return;
    }*/

    function () public payable {
        if (msg.value < orderedProducts[0].price) revert();

        emit LogNewPayment(orderedProducts[0].id,
                            orderedProducts[0].price,
                            1);
    }
}
