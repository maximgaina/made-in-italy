pragma solidity ^0.4.21;

import "./Owned.sol";
import "./Manufacturer.sol";
import "./Retailer.sol";


contract Issuer is Owned {
    event LogNewManufacturer(address certificate);
    event LogNewRetailer(address certificate);

    address[] public manufacturers;
    address[] public retailers;


    function Issuer() public {}

    function deployManufacturer(uint id, string info, uint product)
        public onlyOwner returns (address certificate)
    {
        Manufacturer newMan = new Manufacturer(id, info, product);
        LogNewCertificate(newMan);
        certificates.push(newMan);

        return newCert;
    }

    function getCertificates() public constant returns (address[]) {
        return certificates;
    }

    function getTotalCertificates() public constant returns (uint) {
        return certificates.length;
    }
}
