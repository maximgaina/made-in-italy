pragma solidity ^0.4.21;


contract Owned {
    address public owner;

    modifier onlyOwner {
        if (msg.sender != owner)
            revert();
        _;
    }

    function Owner() public {
        owner = msg.sender;

        return;
    }
}
