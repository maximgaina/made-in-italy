pragma solidity ^0.4.21;


contract Manufacturer {
    event LogNewOrder(string order);

    struct Product {
        string id;
        string name;
        string lot;
        string price;
        uint amount;
        string geolocation;
        /*string organoleptic;*/
        string supplychainId;
    }

    address public creator;

    string public vatNumber;
    string public businessName;
    string public blocation;
    string public postalCode;

    Product[] public products;
    string[] public orders;

    function Manufacturer(
        string vnumber,
        string bname,
        string location,
        string pcode
    )
    public
    {
        creator = msg.sender;

        vatNumber = vnumber;
        businessName = bname;
        blocation = location;
        postalCode = pcode;

        return;
    }

    function addProduct(
        string id,
        string name,
        string lot,
        string price,
        uint amount,
        string geolocation,
        string supplychainId
    )
    public
    {
        /*products[nProducts++] = Product(id, lot, price, amount);*/
        products.push(Product(id, name, lot, price, amount, geolocation, supplychainId));

        return;
    }

    function getCountProducts() public constant returns (uint) {
        return products.length;
    }

    function getProduct(uint index) public constant returns (string, string, string, string, uint) {
        /* No control over index range */
        return (products[index].id,
                products[index].name,
                products[index].lot,
                products[index].price,
                products[index].amount);
    }

    function getTrace(uint index) public constant returns (string, string, string) {
        /* No control over index range */
        return (products[index].lot,
                products[index].geolocation,
                products[index].supplychainId);
    }

    function addOrder(string newOrder) public {
        orders.push(newOrder);
        emit LogNewOrder(newOrder);

        return;
    }

    function getCountOrders() public constant returns (uint) {
        return orders.length;
    }

    function getOrder(uint index) public constant returns (string) {
        return orders[index];
    }
}
