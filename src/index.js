var Web3 = require('web3');

const PREFIX = '0x';

var provider = 'https://ropsten.infura.io/eKV4yUUa3h0PawPo1GEQ';
var web3 = new Web3(new Web3.providers.HttpProvider(provider));
var account = '0x18ee6f47ab374776a7e42c4ff7bf8f8b7e319a98';
var privateKey = 'f885e068b2c4db0e0aedd3442aa7456e8168274bce8321eff94a2d389377aaff';

var manufacturer = require('./../smartcontracts/Manufacturer.sol');
var retailer = require('./../smartcontracts/Retailer.sol');
var deal = require('./../smartcontracts/Deal.sol');


window.dlogic = {
    start: function() {
        web3.eth.accounts.wallet.add(PREFIX + privateKey);

        // this.deployInstance(manufacturer, '1');

        return;
    },

    buildInstance: function(contract) {
        // Warning: check key order when multiple contracts are in the same file
        var abi = contract[Object.keys(contract)[0]].abi;
        var bytecode = contract[Object.keys(contract)[0]].bytecode;

        return new web3.eth.Contract(abi, {data: bytecode});
    },

    deployInstance: async function(contract, ...args) {
        /* TMP */
        var table = document.getElementById('txTable');
        var row;

        var wei, gasLimit, txFee;
        var contractInstance = this.buildInstance(contract);

        var contractData = PREFIX.concat(contractInstance.options.data);

        await Promise.all(
        [
            web3.eth.getGasPrice()
            .then(function(result) {
                wei = result;

                return;
            }),

            web3.eth.estimateGas({ data: contractData, arguments: args })
            .then(function(result) {
                // gasLimit = result;
                gasLimit = result + 100000;

                console.log('gasLimit: ', gasLimit);

                return;
            })
        ]);

        txFee = gasLimit * wei;
        txFee = web3.utils.fromWei(web3.utils.toBN(txFee).toString(), 'ether');

        contractInstance.deploy({ data: contractData, arguments: args })
        .send({ from: account, gas: gasLimit, gasPrice: wei })
        .on('transactionHash', function(transactionHash) {
            console.log(transactionHash);
            row = txTable.insertRow(1);
            row.insertCell(0);
            row.insertCell(0);
            var cell = row.insertCell(0);
            row.cells[0].innerHTML = transactionHash;
        })
        .on('confirmation', function(confirmationNumber, receipt) {
            console.log(confirmationNumber);
            row.cells[2].innerHTML = confirmationNumber;
        })
        .on('receipt', function(receipt) {
            console.log(receipt);
            row.cells[1].innerHTML = receipt.contractAddress;
        })
        .on('error', function(error) {
            console.log(error); })
        .then(function(ret) {
            return ret;
        });

        return;
    }
}

window.addEventListener('load', function() {
    dlogic.start();
});

// https://github.com/brix/crypto-js
