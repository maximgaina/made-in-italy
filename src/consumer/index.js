import "./../style.css";
import "./../index.js";

const privateKey = '011e6558fa9800cfcbf6dd3d1b3b08eed8cd9f535501f4732a23c14048d05e3d';
const PREFIX = '0x';

var manufacturer = require('./../../smartcontracts/Manufacturer.sol');
let manAddr = '0x084d7323cE8fF64f6B8b822C2FfC711415E00f69';

var retailer = require('./../../smartcontracts/Retailer.sol');
let addr = '0x6668E586b0F8e54a316E40F23E077CF1a5eBD0eE';

var deal = require('./../../smartcontracts/Deal.sol');
var dealAddr = '';

var Web3 = require('web3');
var provider = 'https://ropsten.infura.io/eKV4yUUa3h0PawPo1GEQ';
var web3 = new Web3(new Web3.providers.HttpProvider(provider));
const account = 'd64db0b0961c9c3ccc91b3023a0c01dc69e0aed4';


window.consumer = {
    start: function() {
        web3.eth.accounts.wallet.add(PREFIX + privateKey);
        this.updateBalance();

        return;
    },

    updateBalance: async function() {
        web3.eth.getBalance(account).then(function(res) {
            var balance = web3.utils.fromWei(res, 'ether');
            var htmlBalance = document.getElementById('balance');
            htmlBalance.innerHTML = Number(balance).toFixed(3);
        });

        var instance = dlogic.buildInstance(retailer);
        instance.options.address = addr;

        await instance.methods.getCountOrders().call().then(async function(ret) {
            await instance.methods.getOrder(ret - 1).call().then(function(result) {
                dealAddr = result;
                console.log(dealAddr);

                return;
            });

            return;
        });

        var instance = dlogic.buildInstance(deal);
        instance.options.address = dealAddr;

        instance.methods.getProduct(0).call().then(function(ret) {
            var manInstance = dlogic.buildInstance(manufacturer);
            manInstance.options.address = manAddr;

            manInstance.methods.getProduct(0).call()
            .then(function(result) {
                console.log(result);
                var table = document.getElementById('pTable');
                var row = table.insertRow(1);
                var cell = row.insertCell(0);

                cell.innerHTML = result[1];

                return;
            });

            return;
        });

        return;
    },

    pay: async function() {
        var instance = dlogic.buildInstance(retailer);
        instance.options.address = addr;

        await instance.methods.getCountOrders().call().then(async function(ret) {
            await instance.methods.getOrder(ret - 1).call().then(function(result) {
                dealAddr = result;
                console.log(dealAddr);

                return;
            });

            return;
        });

        var instance = dlogic.buildInstance(deal);
        instance.options.address = dealAddr;

        var table = document.getElementById('txTable');
        var row;

        var wei, gasLimit, txFee;
        var contractData = PREFIX.concat(instance.options.data);

        await Promise.all(
        [
            web3.eth.getGasPrice()
            .then(function(result) {
                wei = result;

                return;
            }),

            // instance.methods.sendEther().estimateGas({ from: account, value: '10000000000000000' })
            web3.eth.estimateGas({ from: account, value: '10000000000000000' })
            .then(function(result) {
                gasLimit = result;

                return;
            })
        ]);

        txFee = gasLimit * wei;
        txFee = web3.utils.fromWei(web3.utils.toBN(txFee).toString(), 'ether');

        // instance.methods.sendEther().send({
        web3.eth.sendTransaction({
            from: account,
            gas: gasLimit,
            to: instance.options.address,
            gasPrice: wei,
            value: '10000000000000000' // 0.01 ether
        })
        .on('transactionHash', function(hash) {
            console.log(hash);
            row = txTable.insertRow(1);
            row.insertCell(0);
            row.insertCell(0);
            var cell = row.insertCell(0);
            row.cells[0].innerHTML = hash;
            row.cells[1].innerHTML = instance.options.address;
        })
        .on('receipt', function(receipt) {
            web3.eth.getBalance(account).then(function(res) {
                var balance = web3.utils.fromWei(res, 'ether');
                var htmlBalance = document.getElementById('balance');
                htmlBalance.innerHTML = balance;
            });
        })
        .on('confirmation', function(confirmationNumber, receipt) {
            console.log(confirmationNumber);
            row.cells[2].innerHTML = confirmationNumber;
        })
        .on('error', console.error) // If a out of gas error, the second parameter is the receipt.
        .then(function(ret) { });

        return;
    }
};


window.addEventListener('load', function() {
    consumer.start();

    return;
});
