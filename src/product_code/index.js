import "./../style.css";
import "./../index.js";

var retailer = require('./../../smartcontracts/Retailer.sol');
var retAddr = '0x6668E586b0F8e54a316E40F23E077CF1a5eBD0eE';

var deal = require('./../../smartcontracts/Deal.sol');
var dealAddr;

var Web3 = require('web3');
var socketProvider = 'wss://ropsten.infura.io/ws';
var web3ws = new Web3(new Web3.providers.WebsocketProvider(socketProvider));
var socketInstance;

window.qr = {
    start: async function() {
        var instance = dlogic.buildInstance(retailer);
        instance.options.address = retAddr;

        await instance.methods.getCountOrders().call().then(async function(ret) {
            await instance.methods.getOrder(ret - 1).call().then(function(result) {
                dealAddr = result;
                console.log(dealAddr);
                socketInstance = new web3ws.eth.Contract(deal[Object.keys(deal)[0]].abi, dealAddr);

                return;
            });

            return;
        });

        // var instance = dlogic.buildInstance(deal);
        // instance.options.address = dealAddr;

        socketInstance.events.LogNewPayment((err, events) => {
            console.log(err, events);

            var htmlDiv = document.getElementById('qr');
            var img = document.createElement('img');
            img.id = 'barcode';
            img.src = 'https://api.qrserver.com/v1/create-qr-code/?data=http://vps35355.ovh.net/presentazione/made-in-italy/dist/product_1.html&amp;size=100x100'
            htmlDiv.appendChild(img);

            return;
        });

        return;
    }
}

window.addEventListener('load', function() {
    qr.start();

    return;
});
