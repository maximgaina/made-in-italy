import "./../style.css";
import "./../index.js";


// const privateKey = '011e6558fa9800cfcbf6dd3d1b3b08eed8cd9f535501f4732a23c14048d05e3d';
const PREFIX = '0x';

var manufacturer = require('./../../smartcontracts/Manufacturer.sol');
let manAddr = '0x084d7323cE8fF64f6B8b822C2FfC711415E00f69';

var retailer = require('./../../smartcontracts/Retailer.sol');
let addr = '0x6668E586b0F8e54a316E40F23E077CF1a5eBD0eE';

var deal = require('./../../smartcontracts/Deal.sol');
// let dealAddr = '';


var Web3 = require('web3');
var provider = 'https://ropsten.infura.io/eKV4yUUa3h0PawPo1GEQ';
var web3 = new Web3(new Web3.providers.HttpProvider(provider));
var account = '0x18ee6f47ab374776a7e42c4ff7bf8f8b7e319a98';




window.retail = {
    retrieveInfo: function() {
        var instance = dlogic.buildInstance(retailer);
        instance.options.address = addr;

        instance.methods.vatNumber().call().then(function(ret) {
            var htmlVat = document.getElementById('vat');
            htmlVat.innerHTML = ret;

            return;
        });

        instance.methods.businessName().call().then(function(ret) {
            var htmlName = document.getElementById('name');
            htmlName.innerHTML = ret;

            return;
        });

        instance.methods.blocation().call().then(function(ret) {
            var htmlLoc = document.getElementById('location');
            htmlLoc.innerHTML = ret;

            return;
        });

        instance.methods.postalCode().call().then(function(ret) {
            var htmlPostal = document.getElementById('postalCode');
            htmlPostal.innerHTML = ret;

            return;
        });


        this.updateBalance();

        instance.methods.getCountOrders().call().then(function(ret) {
            var htmlTable = document.getElementById('ordersTable');

            while (htmlTable.rows.length != 1) {
                htmlTable.deleteRow(-1);
            }

            for (var i = 0; i < ret; i++) {
                instance.methods.getOrder(i).call().then(function(result) {
                    var row = htmlTable.insertRow(1);
                    var cell = row.insertCell(0);

                    cell.innerHTML = result;

                    return;
                });
            }

            return;
        });


        return;
    },

    updateBalance: function() {
        web3.eth.getBalance(account).then(function(res) {
            var balance = web3.utils.fromWei(res, 'ether');
            var htmlBalance = document.getElementById('balance');
            htmlBalance.innerHTML = Number(balance).toFixed(3);
        });

        var htmlAddr = document.getElementById('cAddr');
        htmlAddr.innerHTML = addr;

        return;
    },

    traceProduct: function() {
        alert('LOL');

        return;
    },

    searchProducts: async function() {
        var keyword = document.getElementById('search').value;

        var instance = dlogic.buildInstance(manufacturer);
        instance.options.address = manAddr;

        var productsCount;
        await instance.methods.getCountProducts().call()
        .then(function(result) {
            productsCount = result;

            return;
        });

        var manName;
        instance.methods.businessName().call()
        .then(function(result) {
            manName = result;

            return;
        });

        var i;
        var table = document.getElementById('pTable');

        // Deleting current table content
        while (table.rows.length != 1) {
            table.deleteRow(-1);
        }

        for (i = 0; i < productsCount; i++) {
            var str = 'orderAmount';

            await instance.methods.getProduct(i).call()
            .then(function(result) {
                var arr = Object.values(result);

                if (arr[1].search(keyword) != -1) {
                    var j;
                    var row = table.insertRow(1);
                    str = str + (productsCount - i - 1);
                    console.log(str);

                    var input = document.createElement('input');
                    input.type = "text";
                    input.value = '1';
                    input.id = str;
                    input.style.width = "50px";
                    var cell = row.insertCell(0);
                    cell.appendChild(input);

                    var button = document.createElement('button');
                    button.innerHTML = "trace";
                    button.style.width = "60px";
                    button.style.margin = "0px 0px";
                    button.style.padding = "7px 12px";
                    button.addEventListener('click', retail.traceProduct);
                    var cell = row.insertCell(0);
                    cell.appendChild(button);

                    for (j = 0; j < arr.length; j++) {
                        var cell = row.insertCell(0);
                        cell.innerHTML = arr[arr.length - j -1];
                    }

                    var cell = row.insertCell(0);
                    var label = document.createElement('label');
                    label.classList.add('container');
                    label.textContent = manName;
                    var input = document.createElement('input');
                    input.type = 'checkbox';
                    var span = document.createElement('span');
                    span.classList.add('checkmark1');
                    label.appendChild(input);
                    label.appendChild(span);
                    cell.appendChild(label);
                }

                return;
            });
        }

        return;
    },

    order: async function() {
        var table = document.getElementById('pTable');
        // console.log('table length is: ', table.rows.length);
        // console.log(table);

        var ids = [];
        var prices = [];
        var amounts = [];
        var str = 'orderAmount';

        for(var i = 1; i < table.rows.length; i++) {
            if (table.rows[i].cells[0].getElementsByTagName('input')[0].checked) {
                ids.push(table.rows[i].cells[1].innerHTML);
                // prices.push(table.rows[i].cells[4].innerHTML);
                prices.push("1");
                amounts.push(document.getElementById(str.concat(i - 1)).value);
            }
        }
        console.log(ids, prices, amounts);

        var table = document.getElementById('txTable');
        var row;

        var wei, gasLimit, txFee;
        var instance = dlogic.buildInstance(deal);

        var contractData = PREFIX.concat(instance.options.data);

        await Promise.all(
        [
            web3.eth.getGasPrice()
            .then(function(result) {
                wei = result;

                return;
            }),

            web3.eth.estimateGas({ data: contractData, arguments: [ ids, prices, amounts, manAddr ] })
            .then(function(result) {
                // gasLimit = result;
                gasLimit = result + 200000;

                console.log('gasLimit: ', gasLimit);

                return;
            })
        ]);

        txFee = gasLimit * wei;
        txFee = web3.utils.fromWei(web3.utils.toBN(txFee).toString(), 'ether');

        instance.deploy({ data: contractData, arguments: [ ids, prices, amounts, manAddr ] })
        .send({ from: account, gas: gasLimit, gasPrice: wei })
        .on('transactionHash', function(transactionHash) {
            console.log(transactionHash);
            row = txTable.insertRow(1);
            row.insertCell(0);
            row.insertCell(0);
            var cell = row.insertCell(0);
            row.cells[0].innerHTML = transactionHash;
        })
        .on('confirmation', function(confirmationNumber, receipt) {
            console.log(confirmationNumber);
            row.cells[2].innerHTML = confirmationNumber;
        })
        .on('receipt', function(receipt) {
            console.log(receipt);
            row.cells[1].innerHTML = receipt.contractAddress;

            retail.addOrder(receipt.contractAddress);
        })
        .on('error', function(error) {
            console.log(error); })
        .then(function(ret) {

            return ret;
        });

        return;
    },

    addOrder: async function(dealAddress) {
        var retInstance = dlogic.buildInstance(retailer);
        retInstance.options.address = addr;

        var table = document.getElementById('txTable');
        var row;

        var wei, gasLimit, txFee;

        await Promise.all(
        [
            web3.eth.getGasPrice()
            .then(function(result) {
                wei = result;

                return;
            }),

            retInstance.methods.addOrder(dealAddress).estimateGas()
            .then(function(result) {
                // gasLimit = result;
                gasLimit = result + 100000;

                return;
            })
        ]);

        txFee = gasLimit * wei;
        txFee = web3.utils.fromWei(web3.utils.toBN(txFee).toString(), 'ether');

        retInstance.methods.addOrder(dealAddress)
        .send({ from: account, gas: gasLimit, gasPrice: wei })
        .on('transactionHash', function(transactionHash) {
            console.log(transactionHash);
            row = txTable.insertRow(1);
            row.insertCell(0);
            row.insertCell(0);
            var cell = row.insertCell(0);
            row.cells[0].innerHTML = transactionHash;
        })
        .on('confirmation', function(confirmationNumber, receipt) {
            console.log(confirmationNumber);
            row.cells[2].innerHTML = confirmationNumber;
        })
        .on('receipt', function(receipt) {
            console.log(receipt);
            row.cells[1].innerHTML = 'Contract modified';
            retail.retrieveInfo();
            retail.notifyManufacturer(dealAddress);
        })
        .on('error', function(error) {
            console.log(error); })
        .then(function(ret) {});

        return;
    },

    notifyManufacturer: async function(newOrder) {
        var instance = dlogic.buildInstance(manufacturer);
        instance.options.address = manAddr;

        var wei, gasLimit, txFee;
        await Promise.all(
        [
            web3.eth.getGasPrice()
            .then(function(result) {
                wei = result;

                return;
            }),

            instance.methods.addOrder(newOrder).estimateGas()
            .then(function(result) {
                gasLimit = result + 100000;

                return;
            })
        ]);

        txFee = gasLimit * wei;
        txFee = web3.utils.fromWei(web3.utils.toBN(txFee).toString(), 'ether');

        var table = document.getElementById('txTable');
        var row;

        instance.methods.addOrder(newOrder)
        .send({ from: account, gas: gasLimit, gasPrice: wei })
        .on('transactionHash', function(transactionHash) {
            console.log(transactionHash);
            row = txTable.insertRow(1);
            row.insertCell(0);
            row.insertCell(0);
            var cell = row.insertCell(0);
            row.cells[0].innerHTML = transactionHash;
        })
        .on('confirmation', function(confirmationNumber, receipt) {
            console.log(confirmationNumber);
            row.cells[2].innerHTML = confirmationNumber;
        })
        .on('receipt', function(receipt) {
            console.log(receipt);
            row.cells[1].innerHTML = receipt.to;
        })
        .on('error', function(error) {
            console.log(error); })
        .then(function(result) {
            console.log('Success\n', result);

            return;
        });

        return;
    }
};

window.addEventListener('load', function() {
    retail.retrieveInfo();

    return;
});
