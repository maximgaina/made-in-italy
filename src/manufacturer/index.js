import "./../style.css";
import "./../index.js";

var manufacturer = require('./../../smartcontracts/Manufacturer.sol');
let addr = '0x084d7323cE8fF64f6B8b822C2FfC711415E00f69';

var Web3 = require('web3');
var provider = 'https://ropsten.infura.io/eKV4yUUa3h0PawPo1GEQ';
var web3 = new Web3(new Web3.providers.HttpProvider(provider));
var account = '0x18ee6f47ab374776a7e42c4ff7bf8f8b7e319a98';

var orderSubscription;


window.man = {
    retrieveInfo: function() {
        var instance = dlogic.buildInstance(manufacturer);

        instance.options.address = addr;
        console.log(instance);
        console.log(manufacturer);

        var socketProvider = 'wss://ropsten.infura.io/ws';
        var web3ws = new Web3(new Web3.providers.WebsocketProvider(socketProvider));
        var socketInstance = new web3ws.eth.Contract(manufacturer[Object.keys(manufacturer)[0]].abi, addr);
        socketInstance.events.LogNewOrder((err, events)=>{ console.log(err, events)})

        instance.methods.vatNumber().call().then(function(ret) {
            var htmlVat = document.getElementById('vat');
            htmlVat.innerHTML = ret;

            return;
        });

        instance.methods.businessName().call().then(function(ret) {
            var htmlName = document.getElementById('name');
            htmlName.innerHTML = ret;

            return;
        });

        instance.methods.blocation().call().then(function(ret) {
            var htmlLoc = document.getElementById('location');
            htmlLoc.innerHTML = ret;

            return;
        });

        instance.methods.postalCode().call().then(function(ret) {
            var htmlPostal = document.getElementById('postalCode');
            htmlPostal.innerHTML = ret;

            return;
        });

        this.updateBalance();
        this.getProducts();

        instance.methods.getCountOrders().call().then(function(ret) {
            var htmlTable = document.getElementById('ordersTable');

            while (htmlTable.rows.length != 1) {
                htmlTable.deleteRow(-1);
            }

            for (var i = 0; i < ret; i++) {
                instance.methods.getOrder(i).call().then(function(result) {
                    var row = htmlTable.insertRow(1);
                    var cell = row.insertCell(0);

                    cell.innerHTML = result;

                    return;
                });
            }

            return;
        });

        return;
    },

    updateBalance: function() {
        web3.eth.getBalance(account).then(function(res) {
            var balance = web3.utils.fromWei(res, 'ether');
            var htmlBalance = document.getElementById('balance');
            htmlBalance.innerHTML = Number(balance).toFixed(3);
        });

        var htmlAddr = document.getElementById('cAddr');
        htmlAddr.innerHTML = addr;

        return;
    },

    insertProduct: async function() {
        var code = document.getElementById('code').value;
        var name = document.getElementById('pr').value;
        var lot = document.getElementById('lot').value;
        var price = document.getElementById('price').value;
        var amount = document.getElementById('amount').value;
        var geo = document.getElementById('geolocation').value;
        var supplychainId = document.getElementById('supplychain').value;

        console.log(code, name, lot, price, amount);

        /* TMP */
        var table = document.getElementById('txTable');
        var row;

        var instance = dlogic.buildInstance(manufacturer);
        instance.options.address = addr;

        var wei, gasLimit, txFee;

        await Promise.all(
        [
            web3.eth.getGasPrice()
            .then(function(result) {
                wei = result;

                return;
            }),

            instance.methods.addProduct(code, name, lot, price,
                                        amount, geo, supplychainId).estimateGas()
            .then(function(result) {
                // gasLimit = result;
                gasLimit = result + 100000;

                console.log('gasLimit: ', gasLimit);

                return;
            })
        ]);

        txFee = gasLimit * wei;
        txFee = web3.utils.fromWei(web3.utils.toBN(txFee).toString(), 'ether');

        instance.methods.addProduct(code, name, lot, price,
                                    amount, geo, supplychainId)
        .send({ from: account, gas: gasLimit, gasPrice: wei })
        .on('transactionHash', function(transactionHash) {
            console.log(transactionHash);
            row = txTable.insertRow(1);
            row.insertCell(0);
            row.insertCell(0);
            var cell = row.insertCell(0);
            row.cells[0].innerHTML = transactionHash;
        })
        .on('confirmation', function(confirmationNumber, receipt) {
            console.log(confirmationNumber);
            row.cells[2].innerHTML = confirmationNumber;
        })
        .on('receipt', function(receipt) {
            console.log(receipt);
            row.cells[1].innerHTML = receipt.to;
        })
        .on('error', function(error) {
            console.log(error); })
        .then(function(result) {
            console.log('Success\n', result);
            this.getProducts();

            return;
        });

        return;
    },

    getProducts: async function() {
        var instance = dlogic.buildInstance(manufacturer);
        instance.options.address = addr;

        let productsCount;

        await instance.methods.getCountProducts().call()
        .then(function(result) {
            console.log('There are products: ', result);
            productsCount = result;

            return;
        });

        var i;
        var table = document.getElementById('prTable');

        while (table.rows.length != 1) {
            table.deleteRow(-1);
        }

        for (i = 0; i < productsCount; i++) {
            await instance.methods.getProduct(i).call()
            .then(function(result) {
                var arr = Object.values(result);
                // console.log(result, arr);

                var j;
                var row = table.insertRow(1);
                for (j = 0; j < arr.length; j++) {
                    var cell = row.insertCell(0);
                    cell.innerHTML = arr[arr.length - j - 1];
                }

                return;
            });
        }

        return;
    }
};

window.addEventListener('load', function() {
    man.retrieveInfo();
});
