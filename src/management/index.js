import "./../style.css";
import "./../index.js";

var manufacturer = require('./../../smartcontracts/Manufacturer.sol');
var retailer = require('./../../smartcontracts/Retailer.sol');

window.management = {
    deployManufacturer: function() {
        var vatNumber = document.getElementById('vat').value;
        var businessName = document.getElementById('name').value;
        var location = document.getElementById('location').value;
        var postalCode = document.getElementById('postalCode').value;

        console.log(vatNumber, businessName, location, postalCode);

        var radioman = document.getElementById('radioman').checked;
        var radioret = document.getElementById('radioret').checked;

        if (radioman && !radioret) {
            dlogic.deployInstance(manufacturer, vatNumber, businessName, location, postalCode);
        }
        if (!radioman && radioret) {
            dlogic.deployInstance(retailer, vatNumber, businessName, location, postalCode);
        }

        return;
    }
}

window.addEventListener('load', function() {
    // dlogic.deployInstance(manufacturer, '1014', 'Fornitura magia nera', 'Molise', '16899');
});

// function component() {
//     var element = document.createElement('div');
//
//     // Lodash, now imported by this script
//     element.innerHTML = "asd";
//
//     return element;
// }

// Automatic execution and appending
// document.body.appendChild(deployManufacturer());
