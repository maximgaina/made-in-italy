const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: {
        management: './src/management/index.js',
        manufacturer: './src/manufacturer/index.js',
        retailer: './src/retailer/index.js',
        pcode: './src/product_code/index.js',
        consumer: './src/consumer/index.js'
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },

    plugins: [
        new webpack.LoaderOptionsPlugin({
            options: {
                // Solves the problem of requiring 'solc'
                webPreferences: {
                    nodeIntegration: false
                }
            }
        })
    ],

    devServer: {
        contentBase: './dist'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    plugins: ['transform-runtime']
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.sol$/,
                loaders: ['solc-loader']
            }
        ]
    }
};
